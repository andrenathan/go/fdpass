package main

import (
	"log"
	"os"
	"strconv"
	"syscall"

	"gitlab.com/andrenathan/go/fdpass"
)

func readAndPrint(fd int, buf []byte) {
	if _, err := syscall.Read(fd, buf); err != nil {
		log.Fatalf("readAndPrint: %v", err)
	}
	println(string(buf))
}

func getFdFromChild(pid, fd int) {
	var wstatus syscall.WaitStatus
	if _, err := syscall.Wait4(pid, &wstatus, 0, nil); err != nil {
		log.Fatalf("getFdFromChild: wait4 failed: %v", err)
	}
	if wstatus.ExitStatus() != 0 {
		log.Fatalf("openfile exited(%v) with status %v", wstatus.Exited(), wstatus.ExitStatus())
	}
	var smallbuf [10]byte
	var largebuf [4096]byte
	readAndPrint(fd, smallbuf[:])
	recvfd, err := fdpass.Receive(fd)
	if err != nil {
		log.Fatalf("getFdFromChild: fdpass.Receive: %v", err)
	}
	readAndPrint(recvfd, largebuf[:])
	readAndPrint(fd, smallbuf[:])
}

func main() {
	fds, err := syscall.Socketpair(syscall.AF_UNIX, syscall.SOCK_STREAM, 0)
	if err != nil {
		log.Fatalf("mycat: socketpair: %v", err)
	}
	fd0, fd1 := fds[0], fds[1]

	syscall.CloseOnExec(fd0)
	bin := "./openfile/openfile"
	argv := []string{bin, strconv.Itoa(fd1), os.Args[1]}

	pid, err := syscall.ForkExec(bin, argv, nil)
	if err != nil {
		log.Fatalf("getFdFromChild: ForkExec: %v", err)
	}

	syscall.Close(fd1)
	getFdFromChild(pid, fd0)
	syscall.Close(fd0)
}
