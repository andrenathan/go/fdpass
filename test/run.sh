#!/bin/sh

d=`dirname $0`
echo $d
cd $d/mycat && go clean && go build && cd -
cd $d/openfile && go clean && go build && cd -

cd $d
./mycat/mycat ./mycat/mycat.go
